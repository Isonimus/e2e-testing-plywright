import { test, expect } from '@playwright/test';

//EVERY TEST IS ASYNCHRONOUS IN PLAYWRIGHT

test('has title', async ({ page }) => {
  await page.goto('https://playwright.dev/');

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/Playwright/);
});

test('get started link', async ({ page }) => {
  await page.goto('https://playwright.dev/');

  // Click the get started link.
  await page.getByRole('link', { name: 'Get started' }).click();

  // Expects page to have a heading with the name of Installation.
  await expect(page.getByRole('heading', { name: 'Installation' })).toBeVisible();
});

/*
//LOCATORS
test('Playwright locators', async ({ page }) => {
  //MAIN
  page.getByRole("button");
  page.getByText("Welcome, John!");
  page.getByLabel("User name");
  //OTHER
  page.getByPlaceholder("");
  page.getByAltText("");
  page.getByTitle("");
  page.getByTestId(""); //based on data-testid attribute - modifiable in the config
  // ...
});

//ACTIONS
test('Playwright actions', async ({ page }) => {
  const locator = page.getByRole("button"); //EXAMPLE LOCATOR

  //SOME POSSIBLE ACTIONS OVER A GIVEN LOCATOR
  await locator.check()
  await locator.click()
  await locator.uncheck()
  await locator.hover()
  await locator.fill("")
  await locator.focus()
  await locator.press("")
  await locator.setInputFiles("")
  await locator.selectOption("")
  await locator.type("")
  // ...
});

//ASSERTIONS
test('Playwright assertions', async ({ page }) => {
  const locator = page.getByRole("button"); //EXAMPLE LOCATOR

  await expect(locator).toBeChecked()
  await expect(locator).toBeEnabled()
  await expect(locator).toBeVisible()
  await expect(locator).toContainText("")
  await expect(locator).toHaveAttribute("")
  await expect(locator).toHaveCount(2)
  await expect(locator).toHaveText("")
  await expect(locator).toHaveValue("")
  await expect(locator).toHaveScreenshot()
  // ...
});

*/

//
test('First task ', async ({ page }) => {
  await page.goto('https://demo.playwright.dev/todomvc/');

  const input = await page.getByPlaceholder("What needs to be done?")

  await input.dblclick()
  await input.fill("Todo 1")
  await input.press("Enter")
  await input.fill("Todo 2")
  await input.press("Enter")
  // ...
});

