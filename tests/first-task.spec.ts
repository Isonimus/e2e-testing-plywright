import { test, expect, Page } from '@playwright/test';

const todos = ["Todo 1", "Todo 2", "Todo 3"];

async function createDefaultTodos(page: Page){
  const input = await page.getByPlaceholder("What needs to be done?")
  for(const item of todos){
    await input.fill(item)
    await input.press("Enter")
  }
}

test.describe("Starting with Playwright", () => {

  test.beforeEach("Navigate to page", async ({ page }) => {
    await page.goto("/todomvc/");
  })

  test('First task ', async ({ page }) => {
    const input = await page.getByPlaceholder("What needs to be done?")

    let addedTodos: string[] = []

    await input.dblclick()

    for (const [index, todo] of todos.entries()) {
      addedTodos.push(todo)
      await input.fill(todo)
      await input.press("Enter")
      await expect(page.getByTestId("todo-title")).toHaveText(addedTodos);
    }
    // ...
  });

  test('Second task ', async ({ page }) => {
    const input = await page.getByPlaceholder("What needs to be done?")

    let addedTodos: string[] = []

    await input.dblclick()

    for (const [index, todo] of todos.entries()) {
      addedTodos.push(todo)
      await input.fill(todo)
      await input.press("Enter")
      await expect(page.getByTestId("todo-title")).toHaveText(addedTodos);
    }
    // ...
  });

  test('Third task ', async ({ page }) => {
    const input = await page.getByPlaceholder("What needs to be done?")

    let addedTodos: string[] = []

    await input.dblclick()

    for (const [index, todo] of todos.entries()) {
      addedTodos.push(todo)
      await input.fill(todo)
      await input.press("Enter")
      await expect(page.getByTestId("todo-title")).toHaveText(addedTodos);
    }

    /* await input.fill(todos[0])
     await input.press("Enter")
     await expect(page.getByTestId("todo-title")).toHaveText(todos[0]);
   
     await input.fill(todos[1])
     await input.press("Enter")
     await expect(page.getByTestId("todo-title")).toHaveText(todos);*/

    // ...
  });

  test('Fourth task', async ({ page }) => {
    const input = await page.getByPlaceholder("What needs to be done?")

    await input.dblclick()

    await input.fill(todos[0])
    await input.press("Enter")
    await expect(input).toBeEmpty();
    // ...
  });



  test('Fifth task', async ({ page }) => {
    //REFACTORED EVERYTHING INSIDE THE DESCRIBE BLOCK,
    // + ADDED BEFOREEACH HOOK
  });

  test('Sixth task', async ({ page }) => {
    const input = await page.getByPlaceholder("What needs to be done?")

    await input.dblclick()

    for (const [index, todo] of todos.entries()) {
      await input.fill(todo)
      await input.press("Enter")
    }

    //await expect(page.getByTestId("todo-count")).toContainText("3")
    //await expect(page.getByRole("strong")).toHaveText("3") 
    await expect(page.getByRole("strong")).toHaveText(/3/)

  });

  test('Seventh task', async ({ page }) => {
    //INSTALLED VSCODE EXTENSION (PLAYWRIGHT TEST FOR VSCODE)
    //UPDATED playwright.config.ts
    //test.use({locale: "fr-FR"}) //THIS OVERRIDES playwright.config.ts FOR THE TEST
    //IT IS USED IN
  });

  test('Eighth task', async ({ page }) => {
    //MOVED BEFORE EACH URL TO CONFIG FILE:
    //baseURL: "https://demo.playwright.dev/todomvc/",
  });

  test('Ninth task', async ({ page }) => {
    const input = await page.getByPlaceholder("What needs to be done?");
    const selectAllBtn = await page.getByText("Mark all as complete");

    await input.dblclick()

    for (const todo in todos) {
      await input.fill(todo)
      await input.press("Enter")
    }

    await selectAllBtn.check(); //CHECKBOX
    //await expect(page.getByTestId("todo-item").first()).toHaveClass("completed") //JUST FIRST ELEMENT
    await expect(page.getByTestId("todo-item")).toHaveClass(["completed", "completed", "completed"])
  });

})

test.describe("Continuing with Playwright", () => {
  //REFACTOR REDUNDANT CODE TO SEPARATE FUNCTIONS
  test('Tenth task', async ({ page }) => {
    await createDefaultTodos(page);
  });

  test('Eleventh task', async ({ page }) => {
    const selectAllBtn = await page.getByText("Mark all as complete");

    await createDefaultTodos(page);
    await selectAllBtn.check(); //CHECKBOX
    await expect(page.getByTestId("todo-item")).toHaveClass(["completed", "completed", "completed"])
    await selectAllBtn.uncheck(); 
    await expect(page.getByTestId("todo-item")).not.toHaveClass(["completed", "completed", "completed"])
  });

  test('Twelveth task', async ({ page }) => {
    const todoToEdit = await page.getByTestId("todo-title")
    const newText = "This is a new text"

    await createDefaultTodos(page);
    await todoToEdit.dblclick();
    await todoToEdit.fill(newText);
    await expect(todoToEdit).toHaveText(newText);
    
  });
})